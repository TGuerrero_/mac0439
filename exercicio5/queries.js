db = db.getMongo().getDB("mac439");

//1 - Encontre os números dos modelos, o tipo e o preço para todas as impressoras cujo preço está entre R$200,00 e R$300,00.
db.equipamentos.find({
  "tipo": "impressora",
  "preco": {
    $gte: 200.00,
    $lte: 300.00
  }
}, {"modelo":1, "tipo_impressao":1, "preco":1});

//2- Liste todos os fabricantes que fabricam PCs ou impressoras.
db.equipamentos.distinct("fabricante", {"tipo": {$in: ["pc", "impressora"]}});

// 3 - Encontre o número do modelo, a velocidade e o tamanho do HD para os PCs que possuam um CD com velocidade 6x ou 8x e que custem menos que R$2.000,00. O resultado deve aparecer ordenado por ordem decrescente de velocidade e tamanho de HD.
db.equipamentos.find({
  "tipo": "pc",
  "cd": {$in: ["6x", "8x"]},
  "preco": {$lt: 2000.00}  
}, {"modelo":1, "velocidade":1, "hd":1}).sort({"velocidade":-1, "hd":-1});

// 4 - Encontre os modelos dos produtos dos fabricantes cujo nome começa com uma letra que está entre as cinco primeiras do alfabeto. O resultado deve aparecer ordenado em ordem crescente de nome de fabricante.
db.fabricantes.find({
  "fabricante": /^[a-e]/i
}, {"fabricante":1, "equipamentos.modelo":1}).sort({"fabricante":1});

// 5 - Encontre os fabricantes e modelos das impressoras coloridas que não são do tipo ink-jet.
db.equipamentos.find({
  "tipo": "impressora",
  $nor:[{"tipo_impressao": "ink-jet"}],
}, {"fabricante":1, "modelo":1});

// 6 - Liste o fabricante, o modelo e o preço de todos os produtos que aparecem no BD.
db.equipamentos.find({}, {"fabricante":1, "modelo":1, "preco":1});

// 7 - Encontre os fabricantes que vendem tanto impressoras quanto PCs (ou seja, se um fabricante vende PCs mas não vende impressoras, então não deve aparecer na resposta)
db.fabricantes.find({
  $and: [
    {"equipamentos.tipo": "pc"},
    {"equipamentos.tipo": "impressora"}
  ]
}, {"fabricante":1});

// 8 - Liste os fabricantes que vendem computadores (PCs ou Laptops) por mais de R$2.000,00 mas que não vendem impressoras por mais de R$300,00.
db.fabricantes.find({
  "equipamentos": {$elemMatch: {"tipo": {$in: ["pc", "laptop"]}, "preco": {$gt: 2000.00}}},
  $nor: [
    {"equipamentos": {$elemMatch: {"tipo": "impressora", "preco": {$gte: 300.00}}}}
  ]
}, {"fabricante":1});

// 9 - Encontre os tipos de produtos vendidos pelos fabricantes que possuem 3 ou mais palavras em seu nome.
db.fabricantes.distinct("equipamentos.tipo", {
  "fabricante": /^\w+ \w+ \w+.*$/
});

// 10 - Encontre os tipos de produtos vendidos pelos fabricantes que possuem exatamente 2 palavras em seu nome.
db.fabricantes.distinct("equipamentos.tipo", {
  "fabricante": /^\w+ \w+$/
});