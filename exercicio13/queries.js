// 1 - Número de fabricantes por tipo de equipamento
db = db.getMongo().getDB("mac439");

function mapFunctionA() {
  const deviceTypes = new Set();

  this["equipamentos"].forEach((device) => {
    const type = device["tipo"];
    if (!deviceTypes.has(type))
      deviceTypes.add(type);
  });

  for (const type of deviceTypes) {
    emit(type, this["fabricante"]);
  }
};

function reduceFunctionA(_, values) {
  return values.length;
};

db["fabricantes"].mapReduce(mapFunctionA, reduceFunctionA, {
  out: { inline: 1 },
});

// 2 - Preço médio dos equipamentos por tipo de equipamento

function mapFunctionB() {
  this["equipamentos"].forEach((device) => {
    emit(device["tipo"], device["preco"]);
  });
};

function reduceFunctionB(_, values) {
  return Array.sum(values) / values.length;
};

db["fabricantes"].mapReduce(mapFunctionB, reduceFunctionB, {
  out: { inline: 1 },
});

// 3 - Para cada tipo de impressão, devolver o preço da impressora mais barata desse tipo

function mapFunctionC() {
  this["equipamentos"].forEach((device) => {
    if (device["tipo"] == "impressora") {
      emit(device["tipo_impressao"], device["preco"]);
    }
  });
};

function reduceFunctionC(_, values) {
  return values.reduce((acc, current) => Math.min(acc, current));
};

db["fabricantes"].mapReduce(mapFunctionC, reduceFunctionC, {
  out: { inline: 1 },
});

// 4 - Obtenha o preço médio por tipo de equipamento e tamanho de RAM dos computadores (Laptop ou PC) que têm pelo menos 96MB de RAM

function mapFunctionD() {
  this["equipamentos"].forEach((device) => {
    const type = device["tipo"];
    if(type == "pc" || type == "laptop") {
      if(device["ram"] >= 96) {
        emit(
          {
            tipo: type,
            ram: device["ram"],
          },
          device["preco"]
        );
      }
    }
  });
};

function reduceFunctionD(_, values) {
  return Array.sum(values) / values.length;
};

db["fabricantes"].mapReduce(mapFunctionD, reduceFunctionD, {
  out: { inline: 1 },
});
