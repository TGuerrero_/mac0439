-- 1 - Receba um preço como parâmetro de entrada e devolva o modelo do PC cujo preço é o mais próximo.
CREATE OR REPLACE FUNCTION getPc(IN price NUMERIC)
RETURNS PC 
AS $$
  SELECT * 
  FROM pc
  ORDER BY ABS($1 - preco)
  LIMIT 1;
$$ LANGUAGE sql;

-- 2 - Receba um fabricante e um modelo como parâmetros de entrada e devolva o preço do produto (não importando a que tipo de produto o modelo corresponda).
CREATE OR REPLACE FUNCTION productPrice(IN producer CHAR(1), IN model INT)RETURNS NUMERIC(6,2) AS
$$
DECLARE type CHAR(20);
BEGIN
  type := (
    SELECT tipo 
    FROM produto AS p 
    WHERE p.fabricante = producer AND p.modelo = model
  );

  IF type = 'impressora' THEN 
    RETURN (SELECT preco 
            FROM impressora AS i
            WHERE i.modelo = model);
  ELSIF type = 'pc' THEN 
    RETURN (SELECT preco 
            FROM pc AS c
            WHERE c.modelo = model);
  ELSIF type = 'laptop' THEN 
    RETURN (SELECT preco 
            FROM laptop AS l
            WHERE l.modelo = model);
  END IF;
END
$$ LANGUAGE plpgsql;

-- 3 - Receba um modelo, um fabricante, uma velocidade, um tamanho de RAM e de HD como parâmetros de entrada e insira como um novo PC no BD. Entretanto, se já existir um PC (ou um outro produto) com o mesmo número de modelo (problema que será assinalado por meio de uma exceção com SQLSTATE igual a '23000', correspondente a uma violação da restrição de primary key), continue adicionando 1 ao número do modelo até encontrar um número de modelo que ainda não apareça no BD.

CREATE OR REPLACE PROCEDURE createPc(
  IN model INT,
  IN velocity INT,
  IN ram INT,
  IN hd FLOAT
)
AS $$ 
DECLARE currentModel INT := model;
BEGIN
  LOOP 
    BEGIN 
      INSERT INTO pc(modelo, velocidade, ram, hd) 
      VALUES (model, velocity, ram, hd);

    EXCEPTION 
      WHEN SQLSTATE '23000' THEN
        currentModel := currentModel + 1;
        CONTINUE; 
    END;

    EXIT;
  END LOOP;
END; 
$$ LANGUAGE plpgsql;

-- 4 - Dado um preço como parâmetro de entrada, calcule e mostre o número de PCs, o número de laptops e o número de impressoras vendidos por um preço superior ao preço fornecido.
CREATE TYPE Stocks AS (pcs INT, laptops INT, impressoras INT);
CREATE OR REPLACE FUNCTION CountMoreExpensive(IN price NUMERIC(6, 2)) 
RETURNS Stocks
AS $$ 
  DECLARE pcs INT;
  DECLARE laptops INT;
  DECLARE impressoras INT;
BEGIN
  laptops := (
    SELECT COUNT(*) 
    FROM laptop AS l 
    WHERE l.preco > price
  );
  
  impressoras := (
    SELECT COUNT(*)
    FROM impressora AS i 
    WHERE i.preco > price
  );

  pcs := (
    SELECT COUNT(*)
    FROM pc AS p
    WHERE p.preco > price
  );
  RETURN (pcs, laptops, impressoras);
END; 
$$ LANGUAGE plpgsql;

SELECT * FROM getPc(1000);

SELECT * FROM productPrice('F', 3004);

CALL createPc(3000, 666, 10, 1000);

SELECT * FROM CountMoreExpensive(1000);


