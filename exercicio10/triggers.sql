-- a) Na alteração do preço de um PC, caso o preço seja aumentado, garanta que o aumento seja no máximo de 50%. Na tentativa de realização de um aumento superior a 50%, aplique um aumento de apenas 50%.

CREATE OR REPLACE FUNCTION adjustNewPcPrice() RETURNS TRIGGER AS 
  $$
  BEGIN
  NEW.preco = LEAST(NEW.preco, 1.5*OLD.preco);
  RETURN NEW;
  END;
  $$ LANGUAGE plpgsql;

CREATE TRIGGER UpdatePcPrice
BEFORE UPDATE OF preco ON PC
FOR EACH ROW
WHEN (NEW.preco > OLD.preco)
EXECUTE PROCEDURE adjustNewPcPrice();

UPDATE
  pc
SET
  preco = 2*799
WHERE
  modelo = 1001;

-- Deve retornar 1.5*799
SELECT preco FROM pc WHERE modelo = 1001;

-- b) Na inserção ou alteração de uma impressora, só permita que a operação seja realizada se o tipo informado para a impressora for o mesmo de alguma outra impressora já existente na tabela.

CREATE OR REPLACE FUNCTION abortPrinterInsertion() RETURNS TRIGGER AS
  $$
  BEGIN
    IF (NEW.tipo IN (SELECT DISTINCT tipo FROM Impressora)) THEN
      RETURN NEW;
    END IF;
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;

CREATE TRIGGER InsertOrUpdatePrinter
BEFORE INSERT OR UPDATE ON Impressora
FOR EACH ROW
EXECUTE PROCEDURE abortPrinterInsertion();

insert into Produto (fabricante, modelo, tipo) values ('G', 998, 'Impressora');
insert into Produto (fabricante, modelo, tipo) values ('G', 999, 'Impressora');
insert into Impressora (modelo, colorida, tipo, preco) values (998, true,  'mac0439',  231);
insert into Impressora (modelo, colorida, tipo, preco) values (999, true,  'laser',  231);

-- Deve retornar somente a de modelo 999
SELECT * FROM Impressora WHERE modelo = 998 OR modelo = 999;

-- c) Garanta, em todas as circunstâncias que possam causar uma violação, que o preço de um laptop seja limitado ao preço do PC mais caro que houver no BD (ou seja, na tentativa de atribuição de um preço superior ao preço do PC mais caro, substitua-o pelo preço do PC mais caro).

CREATE OR REPLACE FUNCTION adjustLaptopsPrices() RETURNS TRIGGER AS
  $$
  DECLARE newMaxPrice numeric(6,2);
  BEGIN
    newMaxPrice := (SELECT MAX(preco)
              FROM PC);
    UPDATE
      Laptop
    SET
      preco = newMaxPrice
    WHERE
      preco > newMaxPrice;
  RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION adjustNewLaptopPrice() RETURNS TRIGGER AS 
  $$
  DECLARE maxPrice numeric(6,2);
  BEGIN
    maxPrice := (SELECT MAX(preco)
              FROM PC);
  NEW.preco = LEAST(NEW.preco, maxPrice);
  RETURN NEW;
  END;
  $$ LANGUAGE plpgsql;

CREATE TRIGGER adjustLaptopsPricesOnPcUpdateOrDelete
AFTER DELETE OR UPDATE OF preco ON PC
FOR ROW
EXECUTE PROCEDURE adjustLaptopsPrices();

CREATE TRIGGER adjustLaptopPriceOnUpdateOrInsertion
BEFORE INSERT OR UPDATE OF preco ON Laptop
FOR ROW
EXECUTE PROCEDURE adjustNewLaptopPrice();

insert into Produto (fabricante, modelo, tipo) values ('G', 9999, 'Laptop');
insert into Laptop (modelo, velocidade, ram, hd, tela, preco) values (9999, 800,  96, 10, 15.1, 5000);

-- Deve retornar 2499
SELECT preco FROM Laptop WHERE modelo = 9999;
UPDATE
  Pc
SET
  preco = 2000
WHERE
  preco = 2499;

-- Deve retornar 2299
SELECT preco FROM Laptop WHERE modelo = 9999;

-- d) Na inserção de um novo PC, caso ele não esteja cadastrado ainda na tabela Produto, inclua-o em Produto, usando 'H' como fabricante.
CREATE OR REPLACE FUNCTION createNewProduct() RETURNS TRIGGER AS
  $$
  BEGIN
  IF (NOT EXISTS (SELECT * FROM Produto WHERE modelo = NEW.modelo)) THEN
    INSERT INTO Produto (fabricante, modelo, tipo) values ('H', NEW.modelo, 'PC');
  END IF;
  RETURN NEW;
  END;
  $$ LANGUAGE plpgsql;

CREATE TRIGGER insertNewPc
BEFORE INSERT ON PC
FOR EACH ROW
EXECUTE PROCEDURE createNewProduct();

insert into PC (modelo, velocidade, ram, hd, cd, preco) values (11275297, 700, 64, 10, '8x', 799);

-- Deve retornar o produto
SELECT * FROM Produto WHERE modelo = 11275297;

-- e) Garanta, em todas as circunstâncias que podem causar uma violação, que cada fabricante venda no máximo 7 modelos de equipamentos diferentes.
CREATE OR REPLACE FUNCTION validateNewProduct() RETURNS TRIGGER AS
  $$
  DECLARE models INTEGER;
  BEGIN
    models := (SELECT COUNT(*)
              FROM Produto
              WHERE fabricante = NEW.fabricante);
    IF (models > 6) THEN
      RETURN NULL; 
    END IF;
    RETURN NEW;
  END;
  $$ LANGUAGE plpgsql;

CREATE TRIGGER addProduct
BEFORE INSERT ON Produto
FOR EACH ROW
EXECUTE PROCEDURE validateNewProduct();

insert into Produto (fabricante, modelo, tipo) values ('Z', 1100, 'PC');
insert into Produto (fabricante, modelo, tipo) values ('Z', 1101, 'PC');
insert into Produto (fabricante, modelo, tipo) values ('Z', 1102, 'PC');
insert into Produto (fabricante, modelo, tipo) values ('Z', 2100, 'Laptop');
insert into Produto (fabricante, modelo, tipo) values ('Z', 2101, 'Laptop');
insert into Produto (fabricante, modelo, tipo) values ('Z', 2102, 'Laptop');
insert into Produto (fabricante, modelo, tipo) values ('Z', 3100, 'Impressora');
insert into Produto (fabricante, modelo, tipo) values ('Z', 3101, 'Impressora');
insert into Produto (fabricante, modelo, tipo) values ('Z', 3102, 'Impressora');

-- Deve retornar apenas a primeira impressora
SELECT * FROM Produto WHERE fabricante = 'Z' AND tipo = 'Impressora';

-- f) Na remoção de qualquer equipamento da relação Produto, remova o registro correspondente na relação PC, Laptop ou Impressora.

CREATE OR REPLACE FUNCTION removeSpecificRegistry() RETURNS TRIGGER AS
  $$
  BEGIN
  IF (OLD.tipo = 'PC') THEN
    DELETE FROM PC WHERE modelo = OLD.modelo;
  ELSIF (OLD.tipo = 'Impressora') THEN
    DELETE FROM Impressora WHERE modelo = OLD.modelo;
  ELSE
    DELETE FROM Laptop WHERE modelo = OLD.modelo;
  END IF;
  RETURN OLD;
  END;
  $$ LANGUAGE plpgsql;

CREATE TRIGGER removeProduct
BEFORE DELETE ON Produto
FOR EACH ROW
EXECUTE PROCEDURE removeSpecificRegistry();

DELETE FROM Produto WHERE modelo = 1001;

-- Não deve retornar nada
SELECT * FROM PC WHERE modelo = 1001;

-- g) Na remoção de qualquer equipamento das relações PC, Laptop ou Impressora, remova o registro correspondente na relação Produto.

CREATE OR REPLACE FUNCTION removeProduct() RETURNS TRIGGER AS
  $$
  BEGIN
    DELETE FROM Produto WHERE modelo = OLD.modelo;
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;

CREATE TRIGGER removeSpecificRegistry
AFTER DELETE ON PC
FOR EACH ROW
EXECUTE PROCEDURE removeProduct();

CREATE TRIGGER removeSpecificRegistry
AFTER DELETE ON Laptop
FOR EACH ROW
EXECUTE PROCEDURE removeProduct();

CREATE TRIGGER removeSpecificRegistry
AFTER DELETE ON Impressora
FOR EACH ROW
EXECUTE PROCEDURE removeProduct();

DELETE FROM PC WHERE modelo = 1003;
DELETE FROM Laptop WHERE modelo = 2004;
DELETE FROM Impressora WHERE modelo = 3002;

-- Não deve retornar nada
SELECT * FROM Produto WHERE modelo IN (1003, 2004, 3002);

-- 2)
-- a) Não, pois a deleção de cada tupla será feita no máximo uma vez, fazendo com 
-- que os triggers só possam se chamar um número limitado de vezes.
-- 
-- b) Isto é verdade para o item f). Através da restrição  "ON DELETE CASCADE" é possível
-- definir que, após um Produto ser deletado, as tuplas que referenciam o seu modelo nas
-- tabelas filhas serão deletadas também.
-- 
-- Ex: ALTER TABLE PC ADD FOREIGN KEY (modelo) REFERENCES Produto(modelo) ON DELETE CASCADE;