-- a) Dados uma velocidade e uma quantidade de RAM (como parâmetros da função), liste os PCs com a velocidade e RAM dados, mostrando o número do modelo e o preço de cada.
BEGIN TRANSACTION READ ONLY;
	SELECT modelo, preco
	FROM pc
	WHERE velocidade = 700 and ram = 64;
COMMIT;

-- b) Dado um número de modelo, remova a tupla correspondente tanto em PC quanto em Produto.
BEGIN TRANSACTION READ WRITE ISOLATION LEVEL SERIALIZABLE;
	DELETE FROM pc WHERE modelo = 1008;  
	DELETE FROM produto WHERE  modelo = 1008;
COMMIT;

-- c) Dado um número de modelo, diminua o preço desse modelo de PC em R$100.
BEGIN TRANSACTION READ WRITE;
	UPDATE pc
	SET preco = preco-100
	WHERE modelo = 1010;
COMMIT;

-- d) Dado um fabricante, um número de modelo, uma velocidade de processador, um tamanho de RAM, um tamanho de HD, um preço, verifique se já existe um produto com esse número de modelo no BD. Se não existir, insira as informações sobre o modelo nas tabelas PC e Produto.
BEGIN TRANSACTION READ WRITE ISOLATION LEVEL SERIALIZABLE;
	INSERT INTO Produto (fabricante, modelo, tipo)
		SELECT 'H', 1013, 'PC'
		WHERE NOT EXISTS (SELECT * FROM produto WHERE modelo = 1013);

	INSERT INTO pc (modelo, velocidade, ram, hd, cd, preco)
		SELECT 1013,  700,  64, 10, '8x',  799
		WHERE NOT EXISTS (SELECT * FROM Pc WHERE modelo = 1013);
COMMIT;

-- 2)
-- Para os items a e c não deve haver grandes preocupação, somente será necessário
-- realizar a consulta novamente. Para os items b e d, pode haver uma falha caso
-- a tupla seja inserida/removida de uma tabela, mas não na outra, gerando uma
-- possível inconsistência.

-- 3)
-- a - Comportamentos:
-- É possível que o PC retornado não exista mais no bd;
-- É possível que o PC retornado esteja com o seu preço errado;
-- É possível que um novo PC com a mesma velocidade e RAM não seja mostrado no resultado da query.
-- 
-- b - Comportamentos:
-- É possível que uma das as tuplas que fazem referência ao modelo não esteja criada ainda.
-- 
-- c - Comportamentos:
-- É possível que o produto não exista (ou por ter sido deletado, ou não inserido ainda).
-- 
-- d - Comportamentos:
-- É possível que a transação considere erroneamente que existe o produto de modelo específico,
-- mesmo caso o produto esteja sendo deletado.