>> XPath
distinct-values(doc('mondial.xml')/mondial/country/encompassed/@continent)

doc('mondial.xml')/mondial/country[border/@country='BR']/name

doc('mondial.xml')/mondial/country[count(ethnicgroup) > 10]/name

doc('mondial.xml')//river[source/mountains='Andes']/name

>> XQuery
<nomes>
  {
    for $p in doc('mondial.xml')/mondial/country
    order by $p/@area/number() descending
    return $p/name
  }
</nomes>

<countries>
  {
    for $name in doc('mondial.xml')/mondial/country/name
    return <country>{$name/text()}</country>
  }
</countries>

<results>
  {
    let $doc := doc('mondial.xml')
    for $p1 in $doc/mondial/country,
        $p2 in $doc/mondial/country
        where $p1/border/@country = $p2/@car_code
        return 
          <pair>
            {$p1/name}
            {$p2/name} 
          </pair>
  }
</results>

