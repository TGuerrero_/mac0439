db = db.getMongo().getDB("mac439");

// 1 - Obtenha a quantidade de modelos de impressora existentes no BD.
db.produtos.aggregate([
  { $match: { tipo: "impressora" } },
  { $count: "modelos" }
]);

// 2 - Obtenha o modelo e o lojista do produto mais caro e também os do produto mais barato do BD.
db.lojistas.aggregate([
  { $unwind: "$produtos" },
  { $sort: { "produtos.preco": 1 } },
  { $group: 
    {
      _id: null,
      cheap_modelo: { $first: "$produtos.modelo" },
      cheap_lojista: { $first: "$lojista" },
      expensive_modelo: { $last: "$produtos.modelo" },
      expensive_lojista: { $last: "$lojista" },
    }
  },
  { $project: 
    {
      _id: 0, 
      cheapest: { modelo: "$cheap_modelo", lojista: "$cheap_lojista" },
      most_expensive: { modelo: "$expensive_modelo", lojista: "$expensive_lojista" }
    },
  }
]);

// 3 - Obtenha o preço médio dos produtos por lojista. A resposta deve aparecer ordenada pelo preço.
db.lojistas.aggregate([
  { $unwind: "$produtos" },
  { $group: 
    {
      _id: { lojista: "$lojista" },
      avg_price: { $avg: "$produtos.preco" }
    }
  },
  { $sort: { avg_price: 1 } }
]);

// 4 - Obtenha o nome do lojista que vende a maior quantidade de produtos diferentes.
db.lojistas.aggregate([
  { $unwind: "$produtos" },
  { $group: 
    {
      _id: "$lojista",
      modelos: { $count: {} }
    }
  },
  { $sort: { modelos: -1 } },
  { $limit: 1 }
]);

// 5 - Obtenha o preço médio dos produtos por tipo de produto.
db.produtos.aggregate([
  { $group:
    {
      _id: "$tipo",
      avg_price: { $avg: "$preco" }
    }
  }
]);

// 6 - Obtenha o preço médio dos produtos por lojista e tipo de produto.
db.lojistas.aggregate([
  { $lookup:
    {
      from: "produtos",
      localField: "produtos.modelo",
      foreignField: "modelo",
      as: "produtos" 
    }
  },
  { $unwind: "$produtos" },
  { $group:
    {
      _id: { lojista: "$lojista", tipo: "$produtos.tipo" },
      avg_price: { $avg: "$produtos.preco" }
    }
  }
]);

// 7 - Para cada tipo de produto, obtenha a lista dos nomes dos lojistas que vendem esse produto (sem repetições na lista).
db.lojistas.aggregate([
  { $lookup:
    {
      from: "produtos",
      localField: "produtos.modelo",
      foreignField: "modelo",
      as: "produtos" 
    }
  },
  { $unwind: "$produtos" },
  { $group:
    {
      _id: "$produtos.tipo",
      lojistas: { $addToSet: "$lojista" }
    }
  }
]);

// 8 - Obtenha o preço médio dos computadores (Laptop ou PC) que têm pelo menos 128MB de RAM.
db.produtos.aggregate([
  { $match:
    {
      "tipo": {$in: ["laptop", "pc"]},
      "ram": {$gte: 128}
    }
  },
  { $count: "quantidade" }
]);