db = db.getMongo().getDB("mac439");

db.lojistas.insertMany([
{
	"lojista":"Astro do HW e SW",
	"produtos": [
		{"modelo":1001, "preco":799.00},
		{"modelo":1002, "preco":2499.00},
		{"modelo":1003, "preco":1999.00},
		{"modelo":2004, "preco":999.00},
		{"modelo":2005, "preco":2399.00},
		{"modelo":2006, "preco":2999.00}
	]
},
{
	"lojista":"Compra Certa",
	"produtos": [
		{"modelo":1007, "preco":2299.00},
		{"modelo":1008, "preco":999.00},
		{"modelo":2008, "preco":1249.00},
		{"modelo":2009, "preco":2599.00},
		{"modelo":3002, "preco":267.00},
		{"modelo":3003, "preco":390.00},
		{"modelo":3006, "preco":1999.00}
	]
},
{
	"lojista":"Dinastia HW",
	"produtos": [
		{"modelo":1009, "preco":1699.00},
		{"modelo":1010, "preco":699.00},
		{"modelo":1011, "preco":1299.00},
		{"modelo":2007, "preco":3099.00},
	]
},
{
	"lojista":"Equipa Tudo",
	"produtos": [
		{"modelo":1012, "preco":799.00},
		{"modelo":1013, "preco":2499.00},
		{"modelo":2010, "preco":1499.00}
	]
},
{
	"lojista":"Bom de Preço",
	"produtos": [
		{"modelo":2001, "preco":1448.00},
		{"modelo":2002, "preco":2584.00},
		{"modelo":2003, "preco":2738.00}
	]
},
{
	"lojista":"First Class HW",
	"produtos": [
		{"modelo":3001, "preco":231.00},
		{"modelo":3004, "preco":439.00}
	]
},
{
	"lojista":"Gambis HW Solutions",
	"produtos": [
		{"modelo":3005, "preco":200.00}
	]
},
{
	"lojista":"HW House",
	"produtos": [
		{"modelo":3007, "preco":350.00}
	]
},
]);
