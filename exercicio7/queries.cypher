// 1 - Devolva todos os dados dos filmes que foram dirigidos e produzidos por uma mesma pessoa.
MATCH (person:Person) - [:DIRECTED] -> (movie:Movie),
      (person) - [:PRODUCED] -> (movie) 
RETURN movie;

// 2 - Para cada ator, devolva o seu ano de estreia em filmes.
MATCH (person:Person) - [:ACTED_IN] -> (movie:Movie)
RETURN person.name, min(movie.released);

// 3 - Para cada pessoa no BD, devolva a sua lista de conhecidos (ou seja, pessoas que trabalharam em filmes em que ela trabalhou)
MATCH (person:Person) - -> (movie:Movie),
      (coworker:Person) - -> (movie)
RETURN person.name, collect(DISTINCT coworker.name);

// 4 - Mostre os nomes de todos os pares de atores que já atuaram juntos em pelo menos 2 filmes.
MATCH (person1:Person) - [:ACTED_IN] -> (movie:Movie),
      (person2:Person) - [:ACTED_IN] -> (movie)
WITH person1.name AS name1, person2.name as name2, count(movie) AS common_movies 
WHERE common_movies >= 2
RETURN name1, name2;

// 5 - Mostre o nome de cada diretor que já dirigiu dois filmes em sequência sem atuar em nenhum outro filme lançado entre eles.
MATCH (director:Person) - [:DIRECTED] -> (movie1:Movie),
      (director) - [:DIRECTED] -> (movie2:Movie),
      (director) - [:ACTED_IN] -> (movie3:Movie)
WHERE movie1.released < movie2.released 
AND NOT movie3.released IN range(movie1.released, movie2.released)
RETURN DISTINCT director.name;

// 6 - Para cada diretor, mostre o número total de atores que ele dirigiu.
MATCH (director:Person) - [:DIRECTED] -> (movie:Movie),
      (actor:Person) - [:ACTED_IN] -> (movie)
RETURN director.name, count(DISTINCT actor);

// 7 - O Tom Hanks está procurando atores para atuar em um novo filme que ele vai dirigir. Recomende possíveis atores para esse novo filme dele.
// A recomendação foi feita com base nos atores que trabalharam com alguém que trabalhou em algum filme dirigido pelo Tom Hanks.
MATCH (tom:Person {name: "Tom Hanks"}) - [:DIRECTED] -> (movie:Movie),
      (actor:Person) - [:ACTED_IN] -> (movie),
      (actor) - [:ACTED_IN] -> (movie2:Movie),
      (recommendation:Person) - [:ACTED_IN] -> (movie2)
RETURN DISTINCT recommendation;

// 8 - Recomende atores com os quais o Jack Nicholson poderia trabalhar junto (mas que nunca trabalhou antes).
// Mesma estratégia da consulta anterior.
MATCH
      (jack:Person {name: "Jack Nicholson"}) - -> (movie:Movie),
      (person:Person) - -> (movie),
      (person) - -> (movie2:Movie),
      (recommendation:Person) - -> (movie2)
WHERE NOT exists((recommendation) - -> (:Movie) <- - (jack))
RETURN DISTINCT recommendation;