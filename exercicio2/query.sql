SELECT json_build_object('entregas', json_agg(row_to_json(response)))
FROM (
    SELECT 
      (SELECT row_to_json(A) FROM agricultor as A WHERE A.coda = E.coda) as agricultor,
      (SELECT row_to_json(R) FROM restaurante as R WHERE R.codr = E.codr) AS restaurante,
      (SELECT row_to_json(P) from produto as P WHERE P.codp = E.codp) AS produto,
      E.dataentrega as data_entrega, 
      E.qtdequilos AS qtdeQuilos
	  FROM entrega as E
  ) as response