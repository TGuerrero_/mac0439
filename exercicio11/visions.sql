-- 1-a) Uma visão chamada AgricultorCidade, com o código e o nome do agricultor e o nome da cidade onde ele está localizado.
CREATE VIEW AgricultorCidade AS 
SELECT A.CodA, A.NomeA, C.NomeC
FROM Agricultor AS A
JOIN Cidade AS C ON A.CodC = C.CodC;

SELECT * FROM AgricultorCidade;

-- 1-b) Uma visão chamada ProdutosBaratos, com todos os dados de produtos cujo preço por quilo é inferior à média dos preços.
CREATE VIEW ProdutosBaratos AS 
SELECT CodP, NomeP, PrecoQuilo 
FROM Produto
WHERE PrecoQuilo < (SELECT AVG(P.PrecoQuilo) FROM Produto AS P);
  
SELECT * FROM produtosbaratos;

-- 1-c) Uma visão chamada ProdutosRU, com o código, nome e preço dos produtos que já foram entregues pelo menos uma vez para o restaurante RU-USP.
CREATE VIEW ProdutosRU AS 
SELECT DISTINCT P.CodP, P.NomeP, P.PrecoQuilo 
FROM Produto AS P
JOIN Entrega AS E ON E.CodP = P.CodP
JOIN Restaurante AS R ON E.CodR = R.CodR
WHERE R.NomeR = 'RU-USP';

SELECT * FROM ProdutosRU;

-- 1-d) Uma visão chamada RestauranteApoiadorAgriLocal com os dados dos restaurantes que são abastecidos apenas por agricultores da sua cidade.
CREATE VIEW RestauranteApoiadorAgriLocal AS
SELECT RT.NomeR
FROM Restaurante AS RT
WHERE RT.NomeR NOT IN (SELECT R.nomeR
      FROM Restaurante AS R
      JOIN Cidade AS C1 ON C1.CodC = R.CodC
      JOIN Entrega AS E ON R.CodR = E.CodR
      JOIN Agricultor AS A ON A.CodA = E.CodA
      JOIN Cidade AS C2 ON C2.CodC = A.CodC
      WHERE C1.NomeC != C2.NomeC
      GROUP BY R.NomeR);

SELECT * FROM RestauranteApoiadorAgriLocal;

-- e) Uma visão chamada ResumoConsumo que apresenta, para cada restaurante e cada produto, o nome do restaurante, o nome do produto, o total em quilos recebidos do produto pelo restaurante e o preço total correspondente.
CREATE VIEW ResumoConsumo AS
SELECT R.NomeR, P.NomeP, SUM(E.QtdeQuilos) AS total, SUM(E.QtdeQuilos * P.precoQuilo) AS preco
FROM Restaurante AS R
JOIn Entrega AS E ON R.CodR = E.CodR
JOIN Produto AS P ON P.CodP = E.CodP
GROUP BY R.NomeR, P.NomeP;

SELECT * FROM ResumoConsumo;

-- 2-a) Todas as visões que fazem Join não são atualizáveis, o que já descarta a, c, e.
-- Dependendo do SGBD, a visão b pode ser não atualizável também, pois alguns SGBDs não
-- permitem subconsultas.
-- permitem subconsultas.